﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartAssessorApiClient;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Data;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace SmartAssessorClientConsole
{
    class Program
    {
        static string exePath = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;

        static EmailAddressAttribute emailChecker = new EmailAddressAttribute();

        //Get location of log file
        static string sql = string.Format("SELECT value from tblSmartAss_Config WHERE Action = 'GetLogFilePath'");
        static String logFile = Connection.FetchData(sql).Select().First().ItemArray[0].ToString();

        static StreamWriter sw = new StreamWriter(logFile, append: true);

        static void Main(string[] args)
        {
            Console.WriteLine("SmartAssessorApiClient running... this window will close once the application has finished.");

            sw.WriteLine("SmartAssessorApiClient started execution at " + System.DateTime.Now);

            SmartAssessorDataManager.GetAuthToken();

            //Get config to see what to run

            string sql = string.Format("SELECT value from tblSmartAss_Config WHERE Action = 'GetAssessors'");
            if (Connection.FetchData(sql).Select().First().ItemArray[0].ToString() == "True")
            {
                GetAssessors();
                sql = string.Format("Update tblSmartAss_Config set date = '" + System.DateTime.Now + "' where action = 'GetAssessors'");
                ActionResponse arDelete = Connection.DBOps(sql); 
            }

            sql = string.Format("SELECT value from tblSmartAss_Config WHERE Action = 'UpdateAssessors'");
            if (Connection.FetchData(sql).Select().First().ItemArray[0].ToString() == "True")
            {
                UpdateAssessors();
                sql = string.Format("Update tblSmartAss_Config set date = '" + System.DateTime.Now + "' where action = 'UpdateAssessors'");
                ActionResponse arDelete = Connection.DBOps(sql);
            }

            sql = string.Format("SELECT value from tblSmartAss_Config WHERE Action = 'GetEmployers'");
            if (Connection.FetchData(sql).Select().First().ItemArray[0].ToString() == "True")
            {
                GetEmployers();
                sql = string.Format("Update tblSmartAss_Config set date = '" + System.DateTime.Now + "' where action = 'GetEmployers'");
                ActionResponse arDelete = Connection.DBOps(sql);
            }


            sql = string.Format("SELECT value from tblSmartAss_Config WHERE Action = 'GetLearners'");
            if (Connection.FetchData(sql).Select().First().ItemArray[0].ToString() == "True")
            {
                GetLearners();
                sql = string.Format("Update tblSmartAss_Config set date = '" + System.DateTime.Now + "' where action = 'GetLearners'");
                ActionResponse arDelete = Connection.DBOps(sql);
            }


            sql = string.Format("SELECT value from tblSmartAss_Config WHERE Action = 'GetLearnerCourses'");
            if (Connection.FetchData(sql).Select().First().ItemArray[0].ToString() == "True")
            {
                GetLearnerCourses();
                sql = string.Format("Update tblSmartAss_Config set date = '" + System.DateTime.Now + "' where action = 'GetLearnerCourses'");
                ActionResponse arDelete = Connection.DBOps(sql);
            }

            sw.WriteLine("SmartAssessorApiClient completed at " + System.DateTime.Now);
            sw.WriteLine("===============================================================");
            sw.Close();
        }

        static void GetAssessors()
        {
            string sql;
            try
            {
                 JToken content = JToken.Parse(SmartAssessorDataManager.GetSmartAssessorData("Assessor/GetAssessors"));

                 sql = string.Format("DELETE from tblSmartAss_Assessors");
                 ActionResponse arDelete = Connection.DBOps(sql); // SQL response in this object if want to do something in particular
               
                 if (arDelete.ActionSuccessful)
                 {
                    foreach (JToken assessor in content.Children())
                    {
                        //Cater for odd characters that will screw the database
                        string lastName = (string)assessor.SelectToken("LastName");
                        lastName = lastName is null ? "null" : lastName.Replace("O'", "O").Replace("'s", "s");
                        string email = emailChecker.IsValid((string)assessor.SelectToken("Email")) ? (string)assessor.SelectToken("Email") : null;

                        sql = string.Format
                        (
                            @"INSERT into tblSmartAss_Assessors
                            (AssessorId,OrganisationId,Name,FirstName,LastName,MISAssessorId,Email,Phone,Mobile)
                            VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8})",
                            (string)assessor.SelectToken("Id") is null ? "null" : "'" + (string)assessor.SelectToken("Id") + "'",
                            (string)assessor.SelectToken("OrganisationId") is null ? "null" : "'" + (string)assessor.SelectToken("OrganisationId") + "'",
                            (string)assessor.SelectToken("Name") is null ? "null" : "'" + (string)assessor.SelectToken("Name") + "'",
                            (string)assessor.SelectToken("FirstName") is null ? "null" : "'" + (string)assessor.SelectToken("FirstName") + "'",
                            (string)assessor.SelectToken("LastName") is null ? "null" : "'" + lastName + "'",
                            (string)assessor.SelectToken("MISAssessorId") is null ? "null" : "'" + (string)assessor.SelectToken("MISAssessorId") + "'",
                            (string)assessor.SelectToken("Email") is null ? "null" : "'" + email + "'",
                            (string)assessor.SelectToken("Phone") is null ? "null" : "'" + (string)assessor.SelectToken("Phone") + "'",
                            (string)assessor.SelectToken("Mobile") is null ? "null" : "'" + (string)assessor.SelectToken("Mobile") + "'"
                        );
                        ActionResponse arSelect = Connection.DBOps(sql); // SQL response in this object if want to do something in particular
                        if (arSelect.ActionSuccessful == false) {
                            sw.WriteLine("GetAssessors().SmartAssessorApiClient error for Assessor Id " + (string)assessor.SelectToken("Id") + ". Error: " + arSelect.ResponseMessage);
                        }
                    }
                 }
            }
            catch (Exception e)
            {
                sw.WriteLine("GetAssessors() method failed : " + e.Message);
            }
        }


        static void GetEmployers()
        {
            string sql;
            try
            {
                JToken content = JToken.Parse(SmartAssessorDataManager.GetSmartAssessorData("Employer/GetEmployers"));

                sql = string.Format("DELETE from tblSmartAss_Employers");
                ActionResponse arDelete = Connection.DBOps(sql); // SQL response in this object if want to do something in particular

                if (arDelete.ActionSuccessful)
                {
                    foreach (JToken employer in content.Children())
                    {
                        //Cater for errant characters
                        string name = (string)employer.SelectToken("Name");
                        name = name is null ? "null" : name.Replace("O'", "O").Replace("'s","s");
                        string coOrdinatorName = (string)employer.SelectToken("CoordinatorName");
                        coOrdinatorName = coOrdinatorName is null ? "null" : name.Replace("O'", "O").Replace("'s", "s");
                        string address1 = (string)employer.SelectToken("Address1");
                        address1 = address1 is null ? "null" : name.Replace("O'", "O").Replace("'s", "s");

                        sql = string.Format
                        (
                            @"INSERT into tblSmartAss_Employers
                            ([EmployerId],[Name],[MISEmployerId],[EDRSNo],[BusinessDepartment],[BusinessLocation],
                             [BranchCode],[Address1],[Address2],[Address3],[Address4],[Town],[Postcode],[Phone],[CoordinatorName],
                             [CoordinatorEmail],[LIRenewalDate],[HandSassessmentDate],[HandSassessmentRenewalDate])
                            VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18})",
                            (string)employer.SelectToken("Id") is null ? "null" : "'" + (string)employer.SelectToken("Id") + "'",
                            (string)employer.SelectToken("Name") is null ? "null" : "'" + name + "'",
                            (string)employer.SelectToken("MISEmployerId") is null ? "null" : "'" + (string)employer.SelectToken("MISEmployerId") + "'",
                            (string)employer.SelectToken("EDRSNo") is null ? "null" : "'" + (string)employer.SelectToken("EDRSNo") + "'",
                            (string)employer.SelectToken("BusinessDepartment") is null ? "null" : "'" + (string)employer.SelectToken("BusinessDepartment") + "'",
                            (string)employer.SelectToken("BusinessLocation") is null ? "null" : "'" + (string)employer.SelectToken("BusinessLocation") + "'",
                            (string)employer.SelectToken("BranchCode") is null ? "null" : "'" + (string)employer.SelectToken("BranchCode") + "'",
                            (string)employer.SelectToken("Address1") is null ? "null" : "'" + address1 + "'",
                            (string)employer.SelectToken("Address2") is null ? "null" : "'" + (string)employer.SelectToken("Address2") + "'",
                            (string)employer.SelectToken("Address3") is null ? "null" : "'" + (string)employer.SelectToken("Address3") + "'",
                            (string)employer.SelectToken("Address4") is null ? "null" : "'" + (string)employer.SelectToken("Address4") + "'",
                            (string)employer.SelectToken("Town") is null ? "null" : "'" + (string)employer.SelectToken("Town") + "'",
                            (string)employer.SelectToken("Postcode") is null ? "null" : "'" + (string)employer.SelectToken("Postcode") + "'",
                            (string)employer.SelectToken("Phone") is null ? "null" : "'" + (string)employer.SelectToken("Phone") + "'",
                            (string)employer.SelectToken("CoordinatorName") is null ? "null" : "'" + coOrdinatorName + "'",
                            (string)employer.SelectToken("CoordinatorEmail") is null ? "null" : "'" + (string)employer.SelectToken("CoordinatorEmail") + "'",
                            (string)employer.SelectToken("LIRenewalDate") is null ? "null" : "'" + (string)employer.SelectToken("LIRenewalDate") + "'",
                            (string)employer.SelectToken("HandSassessmentDate") is null ? "null" : "'" + (string)employer.SelectToken("HandSassessmentDate") + "'",
                             (string)employer.SelectToken("HandSassessmentRenewalDate") is null ? "null" : "'" + (string)employer.SelectToken("HandSassessmentRenewalDate") + "'"
                        );
                        ActionResponse arSelect = Connection.DBOps(sql); // SQL response in this object if want to do something in particular
                        if (arSelect.ActionSuccessful == false)
                        {
                            sw.WriteLine("GetEmployers().SmartAssessorApiClient error for Employer Id " + (string)employer.SelectToken("Id") + ". Error: " + arSelect.ResponseMessage);

                        }
                    }
                }
            }
            catch (Exception e)
            {
                sw.WriteLine("GetAssessors() method failed : " + e.Message);
            }
        }


        static void GetLearners()
        {
            string sql;
            try
            {
                JToken content = JToken.Parse(SmartAssessorDataManager.GetSmartAssessorData("Learner/GetLearners"));

                sql = string.Format("DELETE from tblSmartAss_Learners");
                ActionResponse arDelete = Connection.DBOps(sql); // SQL response in this object if want to do something in particular

                
                if (arDelete.ActionSuccessful)
                {
                    foreach (JToken learner in content.Children())
                    {
                        //Cater for errant characters
                        string lastName = (string)learner.SelectToken("LastName");
                        lastName = lastName is null ? "null" : lastName.Replace("O'", "O").Replace("'s", "s");
                        string employerName = (string)learner.SelectToken("EmployerName");
                        employerName = employerName is null ? "null" : employerName.Replace("O'", "O").Replace("'s", "s");
                        string email = emailChecker.IsValid((string)learner.SelectToken("Email")) ? (string)learner.SelectToken("Email") : null;

                        sql = string.Format
                        (
                            @"INSERT into tblSmartAss_Learners
                            ([LearnerId],[MISLearnerId],[DateOfBirth],[EmployerId],[EmployerName],[Gender],[LoginId],[FirstName],[LastName],[CountryOfDomicile],
                            [Email],[Phone],[Mobile],[DisabilityCode],[LearningDifficulties],[Address1],[Address2],[Address3],[Address4],[PostCode],[Partner])
                            VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20})",
                            (string)learner.SelectToken("Id") is null ? "null" : "'" + (string)learner.SelectToken("Id") + "'",
                            (string)learner.SelectToken("MISLearnerId") is null ? "null" : "'" + (string)learner.SelectToken("MISLearnerId") + "'",
                            (string)learner.SelectToken("DateOfBirth") is null ? "null" : "'" + (string)learner.SelectToken("DateOfBirth") + "'",
                            (string)learner.SelectToken("EmployerId") is null ? "null" : "'" + (string)learner.SelectToken("EmployerId") + "'",
                            (string)learner.SelectToken("EmployerName") is null ? "null" : "'" + employerName + "'",
                            (string)learner.SelectToken("Gender") is null ? "null" : "'" + (string)learner.SelectToken("Gender") + "'",
                            (string)learner.SelectToken("LoginId") is null ? "null" : "'" + (string)learner.SelectToken("LoginId") + "'",
                            (string)learner.SelectToken("FirstName") is null ? "null" : "'" + (string)learner.SelectToken("FirstName") + "'",
                            (string)learner.SelectToken("LastName") is null ? "null" : "'" + lastName + "'",
                            (string)learner.SelectToken("CountryOfDomicile") is null ? "null" : "'" + (string)learner.SelectToken("CountryOfDomicile") + "'",
                            (string)learner.SelectToken("Email") is null ? "null" : "'" + email + "'",
                            (string)learner.SelectToken("Phone") is null ? "null" : "'" + (string)learner.SelectToken("Phone") + "'",
                            (string)learner.SelectToken("Mobile") is null ? "null" : "'" + (string)learner.SelectToken("Mobile") + "'",
                            (string)learner.SelectToken("Phone") is null ? "null" : "'" + (string)learner.SelectToken("Phone") + "'",
                            (string)learner.SelectToken("DisabilityCode") is null ? "null" : "'" + (string)learner.SelectToken("DisabilityCode") + "'",
                            (string)learner.SelectToken("Address1") is null ? "null" : "'" + (string)learner.SelectToken("Address1") + "'",
                            (string)learner.SelectToken("Address2") is null ? "null" : "'" + (string)learner.SelectToken("Address2") + "'",
                            (string)learner.SelectToken("Address3") is null ? "null" : "'" + (string)learner.SelectToken("Address3") + "'",
                            (string)learner.SelectToken("Address4") is null ? "null" : "'" + (string)learner.SelectToken("Address4") + "'",
                            (string)learner.SelectToken("PostCode") is null ? "null" : "'" + (string)learner.SelectToken("PostCode") + "'",
                            (string)learner.SelectToken("Partner") is null ? "null" : "'" + (string)learner.SelectToken("Partner") + "'"
                        );
                        ActionResponse arSelect = Connection.DBOps(sql); // SQL response in this object if want to do something in particular
                        if (arSelect.ActionSuccessful == false)
                        {
                            sw.WriteLine("GetLearners().SmartAssessorApiClient error for Learner Id " + (string)learner.SelectToken("Id") + ". Error: " + arSelect.ResponseMessage);

                        }
                    }
                }
            }
            catch (Exception e)
            {
                sw.WriteLine("GetLearners() method failed : " + e.Message);
            }
        }

        static void UpdateAssessors()
        {
            try
            {
                //get update data from somewhere... but assume have some for now by hard-coding values.
                
                string json =
                @"{
                    Id: '110e51c2-420b-4505-ad92-bdaf24458f78',
                    OrganisationId: '5a2d87e5-5e0f-11d4-b64f-0090271fb277',
                    Name: 'gmx',
                    AdminLevel: 32768,
                    Email: 'kevinbernard@gmx.co.uk',
                    AdminFlags: 0,
                    Assessment: null,
                    FirstName: 'Kevin',
                    LastName: 'Bernard',
                    AdminType: null,
                    Phone: '07498722071',
                    Mobile: '07498722071',
                    A1Qualification: false,
                    DefaultIV: null,
                    ReadOnly: false,
                    RiskRating: 0,
                    NonQualifiedIV: false
                }";

                string result = SmartAssessorDataManager.PostSmartAssessorData("Assessor/UpdateAssessor", json);
            }
            catch (Exception e)
            {
                sw.WriteLine("UpdateAssessors() method failed : " + e.Message);
            }
        }

        static void GetLearnerCourses()
        {
            string sql;

            try
            {
                JToken content = JToken.Parse(SmartAssessorDataManager.GetSmartAssessorData("Learner/GetLearners"));

                sql = string.Format("DELETE from tblSmartAss_Courses");
                ActionResponse arDelete = Connection.DBOps(sql);

                if (arDelete.ActionSuccessful)
                {
                    sql = string.Format("DELETE from tblSmartAss_LearnerCourse");
                    arDelete = Connection.DBOps(sql);
                }

                if (arDelete.ActionSuccessful)
                {
                    foreach (JToken learner in content.Children())
                    {
                        string strId = (string)learner.SelectToken("Id");
                        JToken courses = JToken.Parse(GetLearnerCourse(strId));

                        foreach (JToken course in courses.Children())
                        {
                            string courseDetails = (string)course.SelectToken("Text");
                            int courseCodeIx = courseDetails.LastIndexOfAny(new char[] { ' ' }) + 1;
                            string courseTitle = courseDetails.Substring(0, courseCodeIx);
                            string courseCode = courseDetails.Substring(courseCodeIx);

                            sql = string.Format
                            (
                                @"INSERT into tblSmartAss_Courses
                                (CourseId,courseCode,courseTitle)
                                VALUES({0},{1},{2})",
                                "'" + (string)course.SelectToken("Id") + "'", "'" + courseCode + "'", "'" + courseTitle + "'"
                            );
                            ActionResponse arSelect = Connection.DBOps(sql);
                            if (arSelect.ActionSuccessful == false && arSelect.ErrorCode != "2627") //There will be duplicates to ignore
                            {
                                sw.WriteLine("GetLearnerCourses() 'INSERT into tblSmartAss_Courses' SmartAssessorApiClient error: " + arSelect.ResponseMessage);
                            }

                            sql = string.Format
                            (
                                @"INSERT into tblSmartAss_LearnerCourse
                                (LearnerId,CourseId)
                                VALUES({0},{1})",
                                "'" + strId + "'", "'" + (string)course.SelectToken("Id") + "'"
                            );
                            arSelect = Connection.DBOps(sql);
                            if (arSelect.ActionSuccessful == false) //There will be duplicates to ignore
                            {
                                sw.WriteLine("GetLearnerCourses() 'INSERT into tblSmartAss_LearnerCourse' SmartAssessorApiClient error: " + arSelect.ResponseMessage);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                sw.WriteLine("GetLearnerCourses() method failed : " + e.Message);
            }
        }
        
        static string GetLearnerCourse(string LearnerId)
        {
            try
            {
                //get update data from somewhere... but assume have some for now by hard-coding values.

                string json =
                @"{Id:'" + LearnerId + "'}";

                return SmartAssessorDataManager.PostSmartAssessorData("Learner/GetCourseNames", json);
            }
            catch (Exception e)
            {
                sw.WriteLine("GetLearnerCourses() method failed : " + e.Message);
                return null;
            }
        }
    }
}
