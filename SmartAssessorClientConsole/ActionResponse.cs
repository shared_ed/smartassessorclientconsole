﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartAssessorClientConsole
{
    public class ActionResponse
    {
        public Boolean ActionSuccessful { get; set; }
        public String ActionDescription { get; set; }
        public String ErrorCode { get; set; }
        public String ResponseMessage { get; set; }

        public ActionResponse()
        {
            ActionSuccessful = true;
            ErrorCode = String.Empty;
            ResponseMessage = String.Empty;
            ActionDescription = String.Empty;
        }
    }
}
