﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace SmartAssessorApiClient
{

    public static class SmartAssessorDataManager
    {
        const string API_VERSION = "APIVersion";
        const string CLIENT_KEY = "ClientKey";
        const string AUTHORIZATION = "Authorization";

        static String authorization = "Bearer ";
               
        public static void GetAuthToken()
        {
            try
            {
                using (var handler = new WebRequestHandler())
                {
                    var postData = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("username", ConfigurationManager.AppSettings["username"]),
                        new KeyValuePair<string, string>("password", ConfigurationManager.AppSettings["password"])
                    };

                    using (var content = new FormUrlEncodedContent(postData))
                    {
                        using (var httpClient = new HttpClient(handler))
                        {
                            using (var response = httpClient.PostAsync(ConfigurationManager.AppSettings["smartAssessorUri"] + "Token", content).Result)
                            {
                                dynamic responseContent = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                                authorization = authorization + Convert.ToString(responseContent.access_token);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static string GetSmartAssessorData(string url)
        {
            try
            {
                using (var webClient = new WebClient())
                { 
                    webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
                    webClient.Headers.Add(API_VERSION, ConfigurationManager.AppSettings["apiVersion"]);
                    webClient.Headers.Add(CLIENT_KEY, ConfigurationManager.AppSettings["clientKey"]);
                    webClient.Headers.Add(AUTHORIZATION, authorization);
                    return webClient.DownloadString(ConfigurationManager.AppSettings["smartAssessorUri"] + url);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static string PostSmartAssessorData(string url, string json)
        {
            try
            {
                using (var webClient = new WebClient())
                {

                    webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                    webClient.Headers.Add(API_VERSION, ConfigurationManager.AppSettings["apiVersion"]);
                    webClient.Headers.Add(CLIENT_KEY, ConfigurationManager.AppSettings["clientKey"]);
                    webClient.Headers.Add(AUTHORIZATION, authorization);
                    return webClient.UploadString(ConfigurationManager.AppSettings["smartAssessorUri"] + url, json);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
