﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Configuration;

namespace SmartAssessorClientConsole    
{
    public class Connection
    {

        public static string ConnectionString()
        {
            return ConfigurationManager.AppSettings["sqlServerConnectionString"];
        }

     
        public static object FetchValue(string sql)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Connection.ConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = sql;
                        cmd.Connection = conn;

                        return cmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static DataTable FetchData(string sql, List<SqlParameter> dbParams = null)
        {
            if (!string.IsNullOrEmpty(sql))
            {
                using (SqlConnection conn = new SqlConnection(Connection.ConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = sql;
                        cmd.Connection = conn;

                        if (dbParams != null)
                        {
                            foreach (SqlParameter sq in dbParams)
                            {
                                cmd.Parameters.Add(sq);
                            }
                        }

                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            DataTable dt = new DataTable();

                            dt.Load(rdr);
                            return dt;
                        }
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static ActionResponse DBOps(string sql)
        {
            return DBOps(sql, CommandType.Text, null);
        }

        public static ActionResponse DBOps(string sql, CommandType commandtype, List<SqlParameter> dbparams)
        {
            ActionResponse ar = new ActionResponse();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Connection.ConnectionString();

                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.CommandType = commandtype;

                    if (dbparams != null)
                    {
                        foreach (SqlParameter sq in dbparams)
                        {
                            cmd.Parameters.Add(sq);
                        }
                    }

                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        ar.ErrorCode = String.Empty;
                        ar.ActionSuccessful = true;
                    }
                    catch (SqlException e)
                    {
                        ar.ActionSuccessful = false;
                        ar.ResponseMessage = e.Message;
                        ar.ErrorCode = e.Number.ToString();
                    }
                    catch (Exception e)
                    {
                        ar.ActionSuccessful = false;
                        ar.ResponseMessage = e.Message;
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return ar;
                }
            }
        }

   

        public Connection()
        {
            //
            // TODO: Add constructor logic here  - if i can think of anything useful
            //
        }
    }
}
